var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var userSchema = new Schema ({
  avatar: {
    type: String,
    default: "https://image.flaticon.com/icons/png/512/78/78373.png"
  },
  username: {
    type: String,
    unique: true,
    required: [true, "Devi inserire lo username"]
  },
  password: {
    type: String,
    required: [true, "Devi inserire la password"]
  },
  categoria: [{
    type: String,
    enum: ["Antipasto", "Primo", "Secondo", "Dolce", "Contorno"],
    required: [true, "Devi inserire la categoria"]
  }],
  ricetta_id: [{
    type: Schema.Types.ObjectId,
    ref: "Ricette"
  }]
});

var Users = mongoose.model("Utenti", userSchema);
module.exports = Users;
