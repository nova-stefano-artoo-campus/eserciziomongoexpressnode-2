var mongoose = require("mongoose");
var Users = require("./Users.model.js");

module.exports = (function() {
  var getUsers = function(req, res) {
    Users.find().populate("ricetta_id").exec().then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  var getUser = function(req, res) {
    var id = req.params.id;
    Users.findById(id).populate("ricetta_id").exec().then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  var createUsers = function(req, res) {
    var newUser = new Users(req.body);
    newUser.save().then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  var deleteUsers = function(req, res) {
    var id = req.params.id;
    Users.findByIdAndRemove(id).then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  var cercaPerCategoria = function(req, res) {
    var categoria = req.query.categoria;
    console.log(categoria);
    Users.find({"categoria": categoria}).exec().then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  var aggiungiCategoria = function(req, res) {
    var id = req.params.id;
    Users.findById(id).exec().then(function(utente) {
      var categoria = req.body;
      utente.categoria.push(categoria.categoria);
      return utente.save();
    }).then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  var rimuoviCategoria = function(req, res) {
    var id = req.params.id;
    Users.findById(id).exec().then(function(utente) {
      var categoria = req.body;
      var indice = utente.categoria.indexOf(categoria.categoria);
      utente.categoria.splice(indice, 1);
      return utente.save();
    }).then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  return {
    getUsers: getUsers,
    getUser: getUser,
    createUsers: createUsers,
    deleteUsers: deleteUsers,
    cercaPerCategoria: cercaPerCategoria,
    aggiungiCategoria: aggiungiCategoria,
    rimuoviCategoria: rimuoviCategoria
  }
})();
