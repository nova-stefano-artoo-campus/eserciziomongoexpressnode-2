var express = require("express");
var router = express.Router();
var Users = require("./Users.controller.js");

router.get("/", Users.getUsers);
router.get("/:id([0-9a-z]{24})", Users.getUser);
router.post("/", Users.createUsers);
router.delete("/:id([0-9a-z]{24})", Users.deleteUsers);
router.get("/cerca", Users.cercaPerCategoria);
router.put("/categoria/:id([0-9a-z]{24})", Users.aggiungiCategoria);
router.put("/rimuovicategoria/:id([0-9a-z]{24})", Users.rimuoviCategoria);

module.exports = router;
