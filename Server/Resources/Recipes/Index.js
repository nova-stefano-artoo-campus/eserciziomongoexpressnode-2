var express = require("express");
var router = express.Router();
var Recipes = require("./Recipes.controller.js");

router.get("/", Recipes.getRecipes);
router.get("/:id([0-9a-z]{24})", Recipes.getRecipe);
router.get("/cerca", Recipes.cercaPerIngrediente);
router.post("/", Recipes.createRecipes);
router.put("/:id([0-9a-z]{24})", Recipes.votoRicetta);
router.put("/commento/:id([0-9a-z]{24})", Recipes.commentoRicetta);
router.delete("/:id([0-9a-z]{24})", Recipes.deleteRecipes);

module.exports = router;
