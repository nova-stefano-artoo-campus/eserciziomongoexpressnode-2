var mongoose = require("mongoose");
var Recipes = require("./Recipes.model.js");

module.exports = (function() {
  var getRecipes = function(req, res) {
    Recipes.find().exec().then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  var getRecipe = function(req, res) {
    var id = req.params.id;
    Recipes.findById(id).exec().then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  var createRecipes = function(req, res) {
    var newRecipe = new Recipes(req.body);
    newRecipe.save().then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  var cercaPerIngrediente = function(req, res) {
    var ingrediente = req.query.ingrediente;
    console.log(ingrediente);
    Recipes.find({"ingredienti": ingrediente}).exec().then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  var votoRicetta = function(req, res) {
    var id = req.params.id;
    var voto = req.body.voto;
    Recipes.findById(id).exec().then(function(ricetta) {
      ricetta.voto.nvoti += 1;
      ricetta.voto.svoti += voto;
      return ricetta.save();
    }).then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  var commentoRicetta = function(req, res) {
    var id = req.params.id;
    Recipes.findById(id).exec().then(function(ricetta) {
      var commento = req.body;
      commento.datacreazione = new Date();
      ricetta.commenti.push(commento);
      return ricetta.save();
    }).then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  var deleteRecipes = function(req, res) {
    var id = req.params.id;
    Recipes.findByIdAndRemove(id).then(function(data) {
      res.status(200).json(data);
    }).catch(function(err) {
      res.status(500).json(err);
    });
  }

  return {
    getRecipes: getRecipes,
    getRecipe: getRecipe,
    createRecipes: createRecipes,
    cercaPerIngrediente: cercaPerIngrediente,
    votoRicetta: votoRicetta,
    commentoRicetta: commentoRicetta,
    deleteRecipes: deleteRecipes
  }
})();
