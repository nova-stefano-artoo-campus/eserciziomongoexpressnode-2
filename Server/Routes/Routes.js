var bodyParser = require("body-parser");

module.exports = function(app, express, path) {

  app.use(bodyParser.json());

  app.use("/users", require("./../Resources/Users"));
  app.use("/recipes", require("./../Resources/Recipes"));
}
