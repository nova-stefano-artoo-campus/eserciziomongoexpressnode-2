var express = require("express");
var path = require("path");
var app = express();
const PORT = 3000;

require("./Config/Database.js");

require("./Routes/Routes.js")(app, express, path);

app.listen(PORT, function() {
  console.log("listening on http://localhost:" + PORT);
});
